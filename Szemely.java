package hazi_9;

public class Szemely {

	private String nev;
	private String telefonSzam;

	public Szemely(String nev, String telefonSzam) {
		this.nev = nev;
		this.telefonSzam = telefonSzam;
	}

	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public String getTelefonSzam() {
		return telefonSzam;
	}

	public void setTelefonSzam(String telefonSzam) {
		this.telefonSzam = telefonSzam;
	}

	@Override
	public String toString() {
		return "Szemely [nev=" + nev + ", telefonSzam=" + telefonSzam + "]";
	}

}
