<h4>Házi Feladat</h4>


- Vegyetek fel a fájl-ba a név mellé telefonszámokat Pl: Pelda Nev;06304567654
- Készítsetek egy Szemely osztályt
- Legyen név és telefonszám metódusa (getterek/setterek)
(a toString metódusa a nevet adja vissza – ez kelleni fog ha a JList-be tesszük bele)
- Mikor olvassuk fel a filet, akkor a Szemely osztály adattagjait töltsük fel
- A JList -nek a modeljét ezekkel az elöbbi lépésben létrehozott Szemely osztályokkal töltsük
- A SzemelyDialog osztályt (gui-t) egészítsük ki úgy, hogy a telefonszámot is módosítani
tudjuk, ehhez fel kell venni egy újabb textFiel-ed
Mielőtt nekiállnátok gondoljátok át, a feladat nem nehéz, csak egyre több változóval
dolgozunk, ezért mielőtt nekiállnátok, gondoljátok át!
Ha bárkinek kérdése van írjatok
