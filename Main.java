package hazi_9;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class Main extends JFrame {

	private JPanel contentPane;
	private JList<Szemely> list;
	private DefaultListModel<Szemely> listModel;
	private String fileUtvonal;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 380);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		// Ezt azért teszem meg itt, mert a ReadFile gombra ezeket megcsináltam,
		// viszont,ha nem akarok beolvasni file-t, hanem csak a listhez szeretnék
		// felvenni
		// elemeket és kiiratni a fileba, akkor a listModel = null (ami elszáll) és a
		// listnek nincs
		// settelve a listModel
		// itt példányosítjuk a DefListModelt
		listModel = new DefaultListModel<>();
		// ide kell a scrollPane, arra figyeljetek, hogy ne a JList komponenst tegyétek
		// majd fel, hanem a JScrollPane-t a felületre, abba lesz a JList
		scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(211, 30, 20, 200);
		contentPane.add(scrollPane);

		JLabel lblSzemlyekLista = new JLabel("Személyek lista");
		lblSzemlyekLista.setHorizontalAlignment(SwingConstants.CENTER);
		lblSzemlyekLista.setBounds(30, 11, 180, 14);
		contentPane.add(lblSzemlyekLista);

		JButton btnReadFile = new JButton("Read file");
		btnReadFile.setBounds(270, 30, 120, 25);
		contentPane.add(btnReadFile);

		JButton btnHozzadmdist = new JButton("Hozzáad/Módosít");
		btnHozzadmdist.setBounds(270, 66, 120, 25);
		contentPane.add(btnHozzadmdist);
		JButton btnKilep = new JButton("Kilép");

		btnKilep.setBounds(270, 295, 120, 25);
		contentPane.add(btnKilep);

		JButton btnWriteFile = new JButton("Write file");
		btnWriteFile.setBounds(270, 102, 120, 25);
		contentPane.add(btnWriteFile);

		JButton btnFileKivlaszt = new JButton("File kiválaszt");
		btnFileKivlaszt.setBounds(270, 253, 120, 25);
		contentPane.add(btnFileKivlaszt);

		list = new JList<Szemely>();
		contentPane.add(list);
		// itt beállítom a JList komponensnek, a listModelt
		list.setModel(listModel);

		list.setBackground(new Color(153, 204, 204));
		list.setBounds(10, 30, 200, 200); // ez a scrollPane lesz :)

		btnReadFile.addActionListener(alReadFile);
		btnHozzadmdist.addActionListener(alHozzMod);
		btnWriteFile.addActionListener(alWriteFile);
		btnFileKivlaszt.addActionListener(alFileVal);
		btnKilep.addActionListener(alExit);
	}

	ActionListener alReadFile = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			BufferedReader br = null;
			listModel = new DefaultListModel<>();
			list.setModel(listModel);
			try {

				br = new BufferedReader(new FileReader(fileUtvonal));
				String sor;
				Szemely szemely;
				while ((sor = br.readLine()) != null) {
					szemely = new Szemely(((sor.split(", ")[0]).substring(13)), (sor.split(", ")[1].substring(12, 23)));
					System.out.println(szemely.toString());

					listModel.addElement(szemely);

				}

			} catch (FileNotFoundException ex) {
				JOptionPane.showMessageDialog(null, ex, "Hiba keletkezett!", JOptionPane.ERROR_MESSAGE);
				System.exit(1);

			} catch (IOException ex) {
				JOptionPane.showMessageDialog(null, ex, "Hiba keletkezett!", JOptionPane.ERROR_MESSAGE);
				System.exit(1);

			} finally {
				try {
					br.close();

				} catch (IOException ex) {
					JOptionPane.showMessageDialog(null, ex, "Hiba keletkezett!", JOptionPane.ERROR_MESSAGE);
					System.exit(1);
				}
			}

		}
	};

	ActionListener alHozzMod = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			SzemelyDialog szd = new SzemelyDialog(listModel, list.getSelectedIndex());
			szd.setVisible(true);

		}
	};

	ActionListener alExit = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);

		}
	};

	ActionListener alWriteFile = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			PrintWriter pw = null;
			try {
				pw = new PrintWriter(fileUtvonal);
				for (int i = 0; i < listModel.getSize(); i++) {
					pw.println(listModel.getElementAt(i));
				}
			} catch (FileNotFoundException ex) {
				JOptionPane.showMessageDialog(null, ex, "Hiba keletkezett!", JOptionPane.ERROR_MESSAGE);
			} finally {
				pw.close();
			}

		}
	};

	ActionListener alFileVal = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fs = new JFileChooser();
			fs.showOpenDialog(null);
			fileUtvonal = fs.getSelectedFile().getPath();
		}
	};
}
