package hazi_9;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class SzemelyDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textFieldNév;
	private JTextField textFieldTelSzam;

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args) {
	// try {
	// SzemelyDialog dialog = new SzemelyDialog();
	// dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	// dialog.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * Create the dialog.
	 */
	public SzemelyDialog(DefaultListModel<Szemely> listModel, Integer index) {
		setBounds(100, 100, 450, 189);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		textFieldNév = new JTextField();
		textFieldNév.setBounds(10, 50, 153, 20);
		contentPanel.add(textFieldNév);
		textFieldNév.setColumns(25);
		// ha index > -1 akkor valahová kattintott, ezért a textFieldNév-be beállítjuk a
		// nevet a listmodelből.
		if (index > -1) {
			textFieldNév.setText(listModel.getElementAt(index).getNev());

		}

		JLabel lblNv = new JLabel("Név:");
		lblNv.setBounds(10, 25, 200, 14);
		contentPanel.add(lblNv);

		textFieldTelSzam = new JTextField();
		textFieldTelSzam.setBounds(220, 49, 153, 22);
		contentPanel.add(textFieldTelSzam);
		textFieldTelSzam.setColumns(10);
		if (index > -1) {
			textFieldTelSzam.setText(listModel.getElementAt(index).getTelefonSzam());

		}

		JLabel lblTelefonszam = new JLabel("Telefonszám:");
		lblTelefonszam.setBounds(222, 24, 116, 16);
		contentPanel.add(lblTelefonszam);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (index > -1) {
							// listModel.set(index, textFieldNév.getText());
							Szemely szem = new Szemely(textFieldNév.getText(), textFieldTelSzam.getText());
							listModel.set(index, szem);
						} else {
							// listModel.addElement(textFieldNév.getText());
							Szemely szem = new Szemely(textFieldNév.getText(), textFieldTelSzam.getText());

							listModel.addElement(szem);
						}

						setVisible(false);

					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
